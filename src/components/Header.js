import React, { Component } from 'react';
import './styles/Header.css';

export default class Header extends Component{
    render(){
        return(
            <header className="mainHeader">
                <div >
                    <h1 className="title">Solumemory</h1>
                </div>
                <div>
                    <button className="btnReset">
                        Reiniciar
                    </button>
                </div>
                <div className="tries">
                    <h3>Intentos:</h3>
                </div>
            </header>
        )
    }
}
